var TreeNode = require("../index.js");
var expect = require("chai").expect;

describe("TreeNode", function() {
    describe("new TreeNode", function () {
        it("creates TreeNode object", function () {
            var node = new TreeNode;

            expect(node.constructor).to.equal(TreeNode);

            expect(node.lastChild).to.equal(null);
            expect(node.firstChild).to.equal(null);
            expect(node.parentNode).to.equal(null);
            expect(node.nextSibling).to.equal(null);
            expect(node.previousSibling).to.equal(null);

            expect(TreeNode.prototype.removeChild).to.be.a('function');
            expect(TreeNode.prototype.insertBefore).to.be.a('function');
        });
    });

    describe("toString()", function () {
        it("checks class name", function () {
            var node = new TreeNode;
            expect(node.toString()).to.equal("[object TreeNode]");
        });
    });

    describe("insertBefore()", function () {
        it("checks inserting logic (without optional parameter)", function () {
            var parent = new TreeNode;
            var node = new TreeNode;

            expect(parent.insertBefore(node)).to.equal(node);

            // Parent node is remains:
            expect(parent.nextSibling).to.equal(null);
            expect(parent.previousSibling).to.equal(null);

            // The node is the only one in children list:
            expect(parent.lastChild).to.equal(node);
            expect(parent.firstChild).to.equal(node);

            // Child node now have parent
            expect(node.parentNode).to.equal(parent);

            expect(node.nextSibling).to.equal(null);
            expect(node.previousSibling).to.equal(null);
        });

        it("tries to insert twice (without optional parameter)", function () {
            var parent = new TreeNode;
            var node = new TreeNode;

            expect(parent.insertBefore(node)).to.equal(node);
            expect(parent.insertBefore(node)).to.equal(null); // not allowed to insert nodes with parent

            // Parent node is remains:
            expect(parent.nextSibling).to.equal(null);
            expect(parent.previousSibling).to.equal(null);

            // The node is the only one in children list:
            expect(parent.lastChild).to.equal(node);
            expect(parent.firstChild).to.equal(node);

            // Child node now have parent
            expect(node.parentNode).to.equal(parent);

            expect(node.nextSibling).to.equal(null);
            expect(node.previousSibling).to.equal(null);
        });

        it("tries to insert to itself (without optional parameter)", function () {
            var parent = new TreeNode;

            expect(parent.insertBefore(parent)).to.equal(null);

            // Parent node is remains:
            expect(parent.lastChild).to.equal(null);
            expect(parent.firstChild).to.equal(null);
            expect(parent.parentNode).to.equal(null);
            expect(parent.nextSibling).to.equal(null);
            expect(parent.previousSibling).to.equal(null);
        });

        it("tries to insert to other parent (node already has parent) (without optional parameter)", function () {
            var parent1 = new TreeNode;
            var parent2 = new TreeNode;
            var node = new TreeNode;

            expect(parent1.insertBefore(node)).to.equal(node);
            expect(parent2.insertBefore(node)).to.equal(null);

            // parent2 node is remains:
            expect(parent2.lastChild).to.equal(null);
            expect(parent2.firstChild).to.equal(null);
            expect(parent2.parentNode).to.equal(null);
            expect(parent2.nextSibling).to.equal(null);
            expect(parent2.previousSibling).to.equal(null);
        });

        it("insert few nodes (without optional parameter)", function () {
            var parent = new TreeNode;
            var node1 = new TreeNode;
            var node2 = new TreeNode;
            var node3 = new TreeNode;

            expect(parent.insertBefore(node1)).to.equal(node1);
            expect(parent.insertBefore(node2)).to.equal(node2); // appends to back
            expect(parent.insertBefore(node3)).to.equal(node3); // appends to back

            expect(parent.lastChild).to.equal(node3);
            expect(parent.firstChild).to.equal(node1);

            expect(node1.parentNode).to.equal(parent);
            expect(node2.parentNode).to.equal(parent);
            expect(node3.parentNode).to.equal(parent);

            expect(node1.nextSibling).to.equal(node2);
            expect(node1.previousSibling).to.equal(null);

            expect(node2.nextSibling).to.equal(node3);
            expect(node2.previousSibling).to.equal(node1);

            expect(node3.nextSibling).to.equal(null);
            expect(node3.previousSibling).to.equal(node2);
        });

        it("insert to child node (without optional parameter)", function () {
            var parent = new TreeNode;
            var node1 = new TreeNode;
            var node2 = new TreeNode;
            var node3 = new TreeNode;

            expect(parent.insertBefore(node1)).to.equal(node1);
            expect(parent.insertBefore(node2)).to.equal(node2);

            expect(node2.insertBefore(node3)).to.equal(node3);

            expect(node2.lastChild).to.equal(node3);
            expect(node2.firstChild).to.equal(node3);

            expect(node3.parentNode).to.equal(node2);
            expect(node3.nextSibling).to.equal(null);
            expect(node3.previousSibling).to.equal(null);
        });

        it("insert before node", function () {
            var parent = new TreeNode;
            var node1 = new TreeNode;
            var node2 = new TreeNode;

            expect(parent.insertBefore(node1)).to.equal(node1);
            expect(parent.insertBefore(node2, node1)).to.equal(node2);

            expect(node2.parentNode).to.equal(parent);

            expect(parent.lastChild).to.equal(node1);
            expect(parent.firstChild).to.equal(node2);

            expect(node1.previousSibling).to.equal(node2);
            expect(node1.nextSibling).to.equal(null);

            expect(node2.previousSibling).to.equal(null);
            expect(node2.nextSibling).to.equal(node1);
        });

        it("insert before node", function () {
            var parent = new TreeNode;
            var node1 = new TreeNode;
            var node2 = new TreeNode;
            var node11 = new TreeNode;
            var node22 = new TreeNode;

            expect(parent.insertBefore(node1)).to.equal(node1);
            expect(parent.insertBefore(node2)).to.equal(node2);

            expect(parent.firstChild).to.equal(node1);
            expect(parent.lastChild).to.equal(node2);

            expect(parent.insertBefore(node11, node1)).to.equal(node11);
            expect(parent.insertBefore(node22)).to.equal(node22);

            expect(parent.firstChild).to.equal(node11);
            expect(parent.lastChild).to.equal(node22);

            expect(node11.previousSibling).to.equal(null);
            expect(node11.nextSibling).to.equal(node1);

            expect(node22.previousSibling).to.equal(node2);
            expect(node22.nextSibling).to.equal(null);

            expect(node1.previousSibling).to.equal(node11);
            expect(node1.nextSibling).to.equal(node2);

            expect(node2.previousSibling).to.equal(node1);
            expect(node2.nextSibling).to.equal(node22);
        });
    });

    describe("removeChild()", function () {
        it("remove the child node the only one child", function () {
            var parent = new TreeNode;
            var node = new TreeNode;

            expect(parent.insertBefore(node)).to.equal(node);

            expect(parent.removeChild(node)).to.equal(node);

            // The node is the only one in children list:
            expect(parent.lastChild).to.equal(null);
            expect(parent.firstChild).to.equal(null);

            expect(node.parentNode).to.equal(null);
        });

        it("remove the child node the only one child", function () {
            var parent = new TreeNode;
            var node = new TreeNode;

            expect(parent.insertBefore(node)).to.equal(node);

            expect(parent.removeChild(node)).to.equal(node);

            // The node is the only one in children list:
            expect(parent.lastChild).to.equal(null);
            expect(parent.firstChild).to.equal(null);

            expect(node.parentNode).to.equal(null);
        });

        it("tries to remove non-belongs child", function () {
            var parent = new TreeNode;
            var node = new TreeNode;

            expect(parent.removeChild(node)).to.equal(null);

            // The node is the only one in children list:
            expect(parent.lastChild).to.equal(null);
            expect(parent.firstChild).to.equal(null);

            expect(node.parentNode).to.equal(null);
        });

        it("checks state after removing first child of two", function () {
            var parent = new TreeNode;
            var node1 = new TreeNode;
            var node2 = new TreeNode;

            expect(parent.insertBefore(node1)).to.equal(node1);
            expect(parent.insertBefore(node2)).to.equal(node2);

            expect(parent.removeChild(node1)).to.equal(node1);

            // The node is the only one in children list:
            expect(parent.lastChild).to.equal(node2);
            expect(parent.firstChild).to.equal(node2);

            expect(node2.nextSibling).to.equal(null);
            expect(node2.previousSibling).to.equal(null);

            expect(node1.nextSibling).to.equal(null);
            expect(node1.previousSibling).to.equal(null);
        });

        it("checks state after removing last child of two", function () {
            var parent = new TreeNode;
            var node1 = new TreeNode;
            var node2 = new TreeNode;

            expect(parent.insertBefore(node1)).to.equal(node1);
            expect(parent.insertBefore(node2)).to.equal(node2);

            expect(parent.removeChild(node2)).to.equal(node2);

            expect(parent.lastChild).to.equal(node1);
            expect(parent.firstChild).to.equal(node1);

            expect(node1.nextSibling).to.equal(null);
            expect(node1.previousSibling).to.equal(null);

            expect(node2.nextSibling).to.equal(null);
            expect(node2.previousSibling).to.equal(null);
        });

        it("checks state after removing middle child of three", function () {
            var parent = new TreeNode;
            var node1 = new TreeNode;
            var node2 = new TreeNode;
            var node3 = new TreeNode;

            expect(parent.insertBefore(node1)).to.equal(node1);
            expect(parent.insertBefore(node2)).to.equal(node2);
            expect(parent.insertBefore(node3)).to.equal(node3);

            expect(parent.removeChild(node2)).to.equal(node2);

            expect(node1.nextSibling).to.equal(node3);
            expect(node1.previousSibling).to.equal(null);

            expect(node3.nextSibling).to.equal(null);
            expect(node3.previousSibling).to.equal(node1);

            expect(node2.nextSibling).to.equal(null);
            expect(node2.previousSibling).to.equal(null);
        });

        it("checks state after removing middle child of three", function () {
            function MyNode(name){
                TreeNode.call(this);
                this.name = name;
                this.data = {};
            }

            var myNodeProto = MyNode.prototype = Object.create(TreeNode.prototype);
            myNodeProto.constructor = MyNode;

            Object.defineProperties(myNodeProto, {
                "removeChild": {
                    "value": function(child){
                        var removedChild = TreeNode.prototype.removeChild.call(this, child);
                        if (removedChild){
                            // Do your stuff here ONLY!
                        }
                    }
                },
                "insertBefore": {
                    "value": function(newChild, targetChild){
                        var insertedNode = TreeNode.prototype.insertBefore.call(this, newChild, targetChild);
                        if (insertedNode){
                            // Do your stuff here ONLY!
                        }
                    }
                }
            });
        });
    });
});
