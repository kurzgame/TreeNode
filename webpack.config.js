"use strict";

var webpack = require("webpack");

module.exports = {
    "entry": "./index.js",
    "module": {
        "loaders": [
            {
                "loader": "strict-loader"
            }
        ]
    },
    "output": {
        "filename": "tree-node.js",
        "library": "TreeNode"
    },
    "watch": true,
    "watchOptions": {
        "aggregateTimeout": 100
    },
	"plugins": [
		new webpack.optimize.UglifyJsPlugin({
			"compress": {
				"warnings": false
			}
		})
	]
};
