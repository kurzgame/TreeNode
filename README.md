# TreeNode (v1.0.1)

Partial implementation of the [Node][] interface. Provides safe & expectable tree-like behaviour.

## Usage

To check everything is OK - execute following commands:

 * ```npm install```
 * ```npm run test``` - to check is everything OK
 * ```npm run webpack``` - to build for front-end usage

## Code example:

Simple example:
```javascript
var parent = new TreeNode;
var child1 = new TreeNode;
var child2 = new TreeNode;
var child11 = new TreeNode;
// ------------------------
parent.insertBefore(child2);
parent.insertBefore(child1, child2);

child1.insertBefore(child11);

// ------------------------
console.log(parent.firstChild);
console.log(parent.lastChild);

console.log(child11.parentNode);
```

Inheritance example:
```javascript
function MyNode(name){
    TreeNode.call(this);
    this.name = name;
    this.data = {};
}

var myNodeProto = MyNode.prototype = Object.create(TreeNode.prototype);
myNodeProto.constructor = MyNode;

Object.defineProperties(myNodeProto, {
    "removeChild": {
        "value": function(child){
            var removedChild = TreeNode.prototype.removeChild.call(this, child);
            if (removedChild){
                // Do your stuff here ONLY!
            }
            return removedChild;
        }
    },
    "insertBefore": {
        "value": function(newChild, targetChild){
            var insertedNode = TreeNode.prototype.insertBefore.call(this, newChild, targetChild);
            if (insertedNode){
                // Do your stuff here ONLY!
            }
            return insertedNode;
        }
    }
});
```

## Browser Capability

Should run well everywhere.

## Feedback

For any questions/propositions/e.t.c you can contact me at <kurzgame@gmail.com>

[Node]: https://developer.mozilla.org/en-US/docs/Web/API/Node
