;void function(){

if (typeof module !== "undefined" && typeof module.exports !== "undefined"){
    module.exports = TreeNode;
} else {
    window.TreeNode = TreeNode;
}

/**
 * Provides simple tree-like functionality, based on the Node interface.
 *
 * @class TreeNode
 * @constructor
 */
function TreeNode() {
    /**
     * @readOnly
     * @property lastChild
     * @type {TreeNode|null}
     */
    this.lastChild = null;
    
    /**
     * @readOnly
     * @property firstChild
     * @type {TreeNode|null}
     */
    this.firstChild = null;
    
    /**
     * @readOnly
     * @property parentNode
     * @type {TreeNode|null}
     */
    this.parentNode = null;
    
    /**
     * @readOnly
     * @property nextSibling
     * @type {TreeNode|null}
     */
    this.nextSibling = null;
    
    /**
     * @readOnly
     * @property previousSibling
     * @type {TreeNode|null}
     */
    this.previousSibling = null;
}

/**
 * @method removeChild
 * @param {TreeNode} child (in/out)
 * @return {TreeNode|null} - removed child or null in case of error
 */
TreeNode.prototype.removeChild = function (child) {
    var prev, next;
    if (child && this === child.parentNode) {
        next = child.nextSibling;
        prev = child.previousSibling;
        // fix possible gaps and memory leaks:
        child.parentNode = null;
        if (prev) {
            prev.nextSibling = next;
        } else {
            // target to remove was a first node;
            // need to update reference on a first node:
            this.firstChild = next;
        }
        if (next) {
            next.previousSibling = prev;
        } else {
            // remove target was last node;
            // need to update reference on a last node:
            this.lastChild = prev;
        }
        child.nextSibling = null;
        child.previousSibling = null;
        return child;
    }
    return null;
};

/**
 * @method insertBefore
 * @param {TreeNode} newChild (in/out)
 * @param {TreeNode} [targetNode] (in/out)
 * @return {TreeNode|null} - inserted child or null in case of error
 */
TreeNode.prototype.insertBefore = function (newChild, targetNode) {
    var node;
    if (this !== newChild && null === newChild.parentNode) {
        newChild.parentNode = this;
        if (targetNode && this === targetNode.parentNode) {
            // targetNode is valid, insert new node before it:
            node = targetNode.previousSibling;
            if (node) {
                // target node is not first and has previous node:
                node.nextSibling = newChild;
                newChild.previousSibling = node;
            } else {
                // target node was first node, need to update reference on a first node:
                this.firstChild = newChild;
            }
            // Bind both target and new nodes together:
            targetNode.previousSibling = newChild;
            newChild.nextSibling = targetNode;
        } else {
            // targetChild is absent/invalid, push new node to the back of the list:
            node = this.lastChild;
            this.lastChild = newChild;
            if (node) {
                node.nextSibling = newChild;
                newChild.previousSibling = node;
            } else {
                // there is no last at the moment (also, first is absent too)
                this.firstChild = newChild;
            }
        }
        return newChild;
    }
    return null;
}

/**
 * @method toString
 * @return {String} - standardized object's class name
 */
TreeNode.prototype.toString = function(){
    return "[object TreeNode]";
};

}();
